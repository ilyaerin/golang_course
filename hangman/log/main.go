package log

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

// Log struct
type Log struct {
	res *os.File
}

type Statistic struct {
	Size   uint
	Games  uint
	Wins   uint
	Letter string
	Word   string
	lettersCounter 	map[string]int
	wordsCounter 	map[string]int
}

// Create a new instanse of Log type
func Init(file string) (*Log, error) {
	os.Mkdir("logs", 0775)
	f, err := os.OpenFile(fmt.Sprintf("%s/%s", "logs", file), os.O_CREATE|os.O_RDWR|os.O_APPEND, 0775)
	if err == nil {
		return &Log{f}, nil
	}
	return nil, err
}

func (l *Log) Stat() *Statistic {
	stat := &Statistic{
		lettersCounter: make(map[string]int),
		wordsCounter: make(map[string]int),
	}

	scanner := bufio.NewScanner(l.res)
	for scanner.Scan() {
		stat.scanLine(scanner.Text())
	}

	stat.Word = maximumElementInMap(stat.wordsCounter)
	stat.Letter = maximumElementInMap(stat.lettersCounter)

	return stat
}

func (stat *Statistic) scanLine(line string) {
	stat.Size++

	if strings.Contains(line, "Player won.") {
		stat.Wins++
	}

	if strings.Contains(line, "Enter letter:") {
		letter := strings.Split(line, ": ")[1]
		if stat.lettersCounter[letter] > 0 {
			stat.lettersCounter[letter]++
		} else {
			stat.lettersCounter[letter] = 1
		}
	}

	if strings.Contains(line, "Game has started.") {
		stat.Games++
		word := strings.Split(line, ": ")[1]
		if stat.wordsCounter[word] > 0 {
			stat.wordsCounter[word]++
		} else {
			stat.wordsCounter[word] = 1
		}
	}
}

func maximumElementInMap(list map[string]int) (s string) {
	maxValue := -1
	for key, value := range list {
		if value > maxValue {
			maxValue = value
			s = key
		}
	}
	return s
}

// Write string to Log
func (l *Log) Write(s string) {
	l.res.WriteString(fmt.Sprintf("%s | %s\n", time.Now().String(), s))
}

// Close resourses of Log
func (l *Log) Close() {
	l.res.Close()
}
