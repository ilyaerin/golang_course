package main

import (
	"hangman/game"
	"testing"
)

func TestNewGame(t *testing.T) {
	g := game.New()

	if g.Active == true {
		t.Error("Expected that new Game should be inactive", g)
	}
	if g.Lives != 7 {
		t.Error("Expected that new Game should have a full lives count", g)
	}
	if g.Win == true {
		t.Error("Expected that new Game shouldn't be Win firstly", g)
	}
}
