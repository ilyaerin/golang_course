package main

import (
	"course/hangman/game"
	"course/hangman/log"
	"flag"
	"fmt"
)

func main() {
	var isStat bool
	flag.BoolVar(&isStat, "stat", false, "Enable statistic mode")
	flag.Parse()

	if isStat {
		showStatistic()
	} else {
		startGame()
	}
}

func startGame() {
	game.Start()
}

func showStatistic() {
	log, _ := log.Init("log.txt")
	defer log.Close()

	stat := log.Stat()
	fmt.Printf("Log size: %d\nGames: %d\nWins: %d\nMost used word: %s\nMost used letter: %s\n", stat.Size, stat.Games, stat.Wins, stat.Word, stat.Letter)
}
