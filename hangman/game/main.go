package game

import (
	"bufio"
	"course/hangman/game/base"
	"fmt"
	"os"
)

func Start() {
	fmt.Println("Hello! It's a Hangman game.\n-")

	g := base.New("log.txt")
	g.Start([]string{})

	fmt.Println("I've taken a random word. Try to guess it.")
	fmt.Println(g.TextStatus())

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		loop(g, scanner.Text())
	}
}

func loop(g *base.Game, input string) {
	switch {
	case input == ":n":
		g.Start([]string{})
	case input == ":h":
		fmt.Println("For exit type ':q', for new game type ':n', for help try ':h'.")
	case input == ":q":
		fmt.Println("Bye! Thank you for the game.")
		os.Exit(0)
	default:
		status, err := g.DoTurn(input)
		if status {
			fmt.Println("It's correct answer")
		} else {
			fmt.Println(err)
		}
		fmt.Println(g.TextStatus())
	}
}
