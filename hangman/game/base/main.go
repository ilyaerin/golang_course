package base

import (
	"course/hangman/log"
	"errors"
	"fmt"
	"math/rand"
	"regexp"
	"strings"
	"time"
)

const BaseLives = 7

var DefaultWords = []string{"orange", "apple", "suffix", "plasma", "home", "detest", "work", "children", "boy"}

type Game struct {
	Started 	bool
	Active      bool
	Win         bool
	Lives       uint
	word        string
	UsedLetters []string
	log         *log.Log
}

func New(logFile string) *Game {
	rand.Seed(time.Now().UTC().UnixNano())

	g := &Game{}
	g.log, _ = log.Init(logFile)
	defer g.log.Close()

	return g
}

func (g *Game) Start(words []string)  {
	if len(words) == 0 {
		words = DefaultWords
	}
	g.word = words[rand.Intn(len(words))];

	g.Started = true
	g.Active = true
	g.Win = false
	g.Lives = BaseLives
	g.UsedLetters = []string{}
	g.log.Write(fmt.Sprintf("Game has started. Word: %s", g.word))
}

func (g *Game) WordLetters() []string {
	letters := []string{}
	for _, c := range g.word {
		l := string(c)
		if contains(g.UsedLetters, l) {
			letters = append(letters, string(l))
		} else {
			letters = append(letters, "_")
		}
	}
	return letters
}

func (g *Game) TextStatus() (msg string) {
	word := strings.Join(g.WordLetters(), " ")
	UsedLetters := strings.Join(g.UsedLetters, ", ")

	switch {
	case g.Win:
		msg = fmt.Sprintf("You've won! Word: %s\nUsed letters: %s\n", word, UsedLetters)
	case !g.Active:
		msg = fmt.Sprintf("The game haven't started yet!.")
	case g.Lives == 0:
		msg = fmt.Sprintf("You've died(\nGame is ended!\nThe word was: %s\nYou're world: %s\nUsed letters: %s\n", g.word, word, UsedLetters)
	default:
		msg = fmt.Sprintf("Word: %s\nUsed letters: %s\nYou have %d lives.", word, UsedLetters, g.Lives)
	}
	return
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func (g *Game) CheckLetter(input string) bool {
	for _, a := range g.word {
		if string(a) == input {
			return true
		}
	}
	return false
}

func (g *Game) сheckInput(input string) error {
	if contains(g.UsedLetters, input) {
		return errors.New("You're already used this letter")
	}

	if len(input) > 1 {
		return errors.New("You can ask only a single letter")
	}

	if len(input) > 1 {
		return errors.New("You can ask only a single letter")
	}

	if !regexp.MustCompile(`^[a-z]{1}$`).MatchString(input) {
		return errors.New("You can use only a-z letters")
	}
	return nil
}

func (g *Game) DoTurn(input string) (bool, error) {
	if !g.Active {
		return false, errors.New("The Game isn't active")
	}

	if error := g.сheckInput(input); error != nil {
		return false, error
	}

	if g.Lives > 1 {
		g.log.Write(fmt.Sprintf("Enter letter: %s", input))
		g.UsedLetters = append(g.UsedLetters, input)

		if g.CheckLetter(input) {
			if strings.Join(g.WordLetters(), "") == g.word {
				if !g.Win && g.Active {
					g.log.Write(fmt.Sprintf("Game has ended. Player won. Lives: %d.", g.Lives))
				}
				g.Win = true
				g.Active = false
			}

			return true, nil
		} else {
			g.Lives--
			return false, errors.New("It's a wrong answer")
		}
	} else {
		g.Active = false
	}

	return false, errors.New("You have 0 lives")
}
