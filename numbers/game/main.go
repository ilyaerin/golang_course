package game

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
	"math"
)

const MaxNumber = 20

type Game struct {
	number int
}

func New() {
	rand.Seed(time.Now().UTC().UnixNano()) // We need this line for getting really random values for each execution of application

	g := Game{}
	g.startGame()

	fmt.Println("It's a number game!\n--")
	fmt.Printf("I've taken a random number from 1 to %v. Try to guess it. For exit type 'q', for new game type 'n', for help try 'h'.\n", MaxNumber)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		loop(scanner.Text(), &g)
	}
}

func loop(input string, g *Game) {
	switch {
	case input == "n":
		g.startGame()
		fmt.Println("New game has started.")
	case input == "h":
		fmt.Println("For exit type 'q', for new game type 'n', for help try 'h'.")
	case input == "q":
		fmt.Println("Bye! Thank you for the game.")
		os.Exit(0)
	default:
		g.checkAnswer(input)
	}
}

func (g *Game) startGame() {
	g.number = rand.Intn(MaxNumber)
}

func (g *Game) checkAnswer(input string) {
	value, _ := strconv.Atoi(input)
	abs := math.Abs(float64(value - g.number))

	switch {
	case value == g.number:
		fmt.Println("You win! It's a correct answer!")
		g.startGame()
		fmt.Println("Lets try again! New game has started.")
	case abs < MaxNumber / 10:
		fmt.Println("It's wrong, but you are so close. Don't stop.")
	case abs < MaxNumber / 5:
		fmt.Println("It's wrong, but you are close. Try again.")
	case value < g.number:
		fmt.Println("It's wrong. Your number is lower. Try again.")
	case value > g.number:
		fmt.Println("It's wrong. Your number is more. Try again.")
	}
}
