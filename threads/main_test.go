package main

import (
	"testing"
)

// go test -bench=.

func BenchmarkServerWorkers1(b *testing.B)  { benchmarkServer(1, b) }
func BenchmarkServerWorkers2(b *testing.B)  { benchmarkServer(2, b) }
func BenchmarkServerWorkers10(b *testing.B) { benchmarkServer(10, b) }

func benchmarkServer(workers int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		StartServer(workers, "commands/benchmark.txt")
	}
}
