package main

import (
	"bufio"
	"course/threads/queue"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"sync"
)

var activeWorkers int

func main() {
	var workers int
	var config string
	flag.IntVar(&workers, "w", 3, "Count of workers")
	flag.StringVar(&config, "c", "", "Path to configuration file")
	flag.Parse()

	StartServer(workers, config)
}

func StartServer(workers int, config string) {
	jobs := make(chan queue.Job, 999)
	results := make(chan queue.Result, 999)
	var wg sync.WaitGroup

	// Start workers
	for i := 0; i < workers; i++ {
		activeWorkers += 1
		go queue.Worker(i, jobs, results, &wg)
	}

	if config == "" {
		startConsoleLoop(&wg, jobs, results) // Start loop of commands
	} else {
		startConfigLoop(config, &wg, jobs, results) // Start loops for commands from config file
	}
}

func startConfigLoop(config string, wg *sync.WaitGroup, jobs chan queue.Job, results chan queue.Result) {
	id := 0
	for _, line := range readConfig(config) {
		loop(id, line, wg, jobs, results)
		id++
	}
	wg.Wait()
}

func startConsoleLoop(wg *sync.WaitGroup, jobs chan queue.Job, results chan queue.Result) {
	id := 0
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		loop(id, scanner.Text(), wg, jobs, results)
		id++
	}
}

func loop(id int, input string, wg *sync.WaitGroup, jobs chan queue.Job, results chan queue.Result) {
	params := strings.Split(input, " ")

	switch params[0] {
	case "time":
		addWg(wg)
		jobs <- queue.Job{id, "time", map[string]string{}}
	case "sleep":
		addWg(wg)
		seconds := "5"
		if len(params) == 2 {
			seconds = params[1]
		}
		jobs <- queue.Job{id, "sleep", map[string]string{"seconds": seconds}}
	case "kill":
		addWg(wg)
		activeWorkers -= 1
		jobs <- queue.Job{id, "kill", map[string]string{}}
	case "createFile":
		if len(params) == 2 && len(params[1]) != 0 {
			addWg(wg)
			jobs <- queue.Job{id, "createFile", map[string]string{"fileName": params[1]}}
		} else {
			log.Println("Empty file name")
		}
	case "readFile":
		if len(params) == 2 && len(params[1]) != 0 {
			addWg(wg)
			jobs <- queue.Job{id, "readFile", map[string]string{"fileName": params[1]}}
		} else {
			log.Println("Empty file name or count of lines")
		}
	case "writeFile":
		if len(params) == 3 && len(params[1]) != 0 {
			addWg(wg)
			jobs <- queue.Job{id, "writeFile", map[string]string{"fileName": params[1], "string": params[2]}}
		} else {
			log.Println("Empty file name or string for writing")
		}
	case "exit":
		wg.Wait()
		printResults(jobs, results)
		os.Exit(0)
	default:
		log.Println("Wrong command")
	}
}

func addWg(wg *sync.WaitGroup) {
	if activeWorkers > 0 {
		wg.Add(1)
	}
}

func readConfig(configFile string) (configs []string) {
	file, err := os.Open(configFile)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		configs = append(configs, scanner.Text())
	}
	return configs
}

func printResults(jobs chan queue.Job, results chan queue.Result) {
	close(jobs)
	fmt.Println("\nPlanned jobs:")
	for job := range jobs {
		fmt.Println(job)
	}

	close(results)
	fmt.Println("\nResults:")
	for result := range results {
		fmt.Println(result)
	}
}
