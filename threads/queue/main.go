package queue

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"
	"time"
)

type Job struct {
	Id      int
	Command string
	Params  map[string]string
}

type Result struct {
	Id     int
	Params map[string]string
}

func Worker(id int, jobs chan Job, results chan Result, wg *sync.WaitGroup) {
	// log.Println("Worker", id, "has started")
	for {
		job, more := <-jobs
		if more {
			// log.Println("Worker", id, "got job #", job.Id, "with command", job.Command, "and params", job.Params)
			switch job.Command {
			case "time":
				results <- job.timeCommand()
			case "sleep":
				results <- job.sleepCommand()
			case "createFile":
				results <- job.createFileCommand()
			case "readFile":
				results <- job.readFileCommand()
			case "writeFile":
				results <- job.writeFileCommand()
			case "kill":
				results <- job.killCommand(id)
				wg.Done()
				return
			default:
				log.Println("Wrong command")
			}
			wg.Done()
			// log.Println("Job", job.Id, "is done.", result)
		}
	}
}

func (job *Job) timeCommand() Result {
	fmt.Println(time.Now())
	return job.makeResult("Done")
}

func (job *Job) sleepCommand() Result {
	fmt.Println(time.Now())
	seconds, _ := strconv.Atoi(job.Params["seconds"])
	time.Sleep(time.Second * time.Duration(seconds))
	fmt.Println(time.Now())
	return job.makeResult("Done")
}

func (job *Job) createFileCommand() Result {
	if _, err := os.Create(job.Params["fileName"]); err != nil {
		log.Println(err)
		return job.makeResult(fmt.Sprintf("File %s hasn't created. Error: %s", job.Params["fileName"], err))
	} else {
		return job.makeResult(fmt.Sprintf("File %s has created", job.Params["fileName"]))
	}
}

func (job *Job) writeFileCommand() Result {
	file, err := os.OpenFile(job.Params["fileName"], os.O_RDWR|os.O_APPEND, 0775)
	defer file.Close()

	if err != nil {
		log.Println(err)
		return job.makeResult(fmt.Sprintf("String %s hasn't written to file. Error: %s", job.Params["fileName"], err))
	} else {
		file.WriteString(fmt.Sprintf("%s\n", job.Params["string"]))
		return job.makeResult(fmt.Sprintf("String has written to file %s", job.Params["fileName"]))
	}
}

func (job *Job) readFileCommand() Result {
	file, err := os.OpenFile(job.Params["fileName"], os.O_RDONLY, 0775)
	defer file.Close()

	if err != nil {
		log.Println(err)
		return job.makeResult(fmt.Sprintf("Can't read line from file %s. Error: %s", job.Params["fileName"], err))
	} else {
		scanner := bufio.NewScanner(file)
		lines := []string{}
		for scanner.Scan() {
			lines = append(lines, scanner.Text())
		}
		fmt.Println(lines)
		return job.makeResult(fmt.Sprintf("Data have readed from file %s: %v", job.Params["fileName"], lines))
	}
}

func (job *Job) killCommand(id int) Result {
	fmt.Printf("Worker %d has killed\n", id)
	return job.makeResult(fmt.Sprintf("Worker %d has killed", id))
}

func (job *Job) makeResult(result string) Result {
	return Result{job.Id, map[string]string{"Command": job.Command, "Result": result}}
}
