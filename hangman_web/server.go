package main

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/contrib/sessions"
	"net/http"
	"course/hangman/game/base"
	"strings"
	"fmt"
	"io/ioutil"
	"net/url"
	"time"
)

const MIN_WORDS_COUNT = 3

var words = []string{}
var game = base.New("log.txt")

func main() {
	go loadingWords()

	router := gin.Default()
	router.LoadHTMLGlob("templates/*")
	router.Static("/static", "./static")

	store := sessions.NewCookieStore([]byte("random_secret_string"))
	router.Use(sessions.Sessions("hamgman_session", store))

	// --------------------------------------------- GET / ------------------------------------------- //
	router.GET("/", index)

	// ------------------------------------------- POST /new ------------------------------------------- //
	router.POST("/new", new)

	// ------------------------------------------- POST /do ------------------------------------------- //
	router.POST("/do", do)

	router.Run() // listen and server on 0.0.0.0:8080
}

func index(c *gin.Context) {
	session := sessions.Default(c)
	error := session.Get("error")
	message := session.Get("message")

	session.Delete("error")
	session.Delete("message")
	session.Save()

	c.HTML(http.StatusOK, "index.tmpl", gin.H{
		"game": game,
		"word": strings.Join(game.WordLetters(), " "),
		"usedLetters": strings.Join(game.UsedLetters, ", "),
		"error": error,
		"message": message,
	})
}

func new(c *gin.Context) {
	session := sessions.Default(c)

	if len(words) >= MIN_WORDS_COUNT {
		game.Start(words)
		words = []string{}
		session.Set("message", "The Game has starged. Good luck!")
	} else {
		session.Set("error", "The dictionary has loading now. Try to start a new game later.")
	}
	session.Save()

	c.Redirect(http.StatusMovedPermanently, "/")
}

func do(c *gin.Context) {
	session := sessions.Default(c)

	message, error := game.DoTurn(c.PostForm("letter"))
	if error != nil { session.Set("error", fmt.Sprint(error)) }
	if message { session.Set("message", "It's a correct answer") }
	session.Save()

	c.Redirect(http.StatusMovedPermanently, "/")
}

func loadingWords() {
	for {
		if len(words) < MIN_WORDS_COUNT {
			resp, err := http.PostForm("http://watchout4snakes.com/wo4snakes/Random/RandomWord", url.Values{})
			if err != nil { return }

			defer resp.Body.Close()

			body, err := ioutil.ReadAll(resp.Body)
			if err != nil { return }

			words = append(words, string(body))

			fmt.Println(words)
		}
		time.Sleep(1 * time.Second) // This timeout is needed only to show words loading process.
	}
}